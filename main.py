import os
from sys import argv
from functools import reduce
from random import uniform
from collections import defaultdict
from graphic import display_result

def load_training_data_from_file(training_data_filename):
    result = []
    with open(training_data_filename, 'r') as input_file:
        for line in input_file.readlines():
            items = line.split()
            vector = list(map(int, items[:2]))
            decision = items[2] == 'True'
            result.append((vector, decision))
    return result

ERMIT_COEFFICIENTS = [1, 4, 4, 16]

SUBSTITUTION_INDEXES = {
    1: [0],
    2: [1],
    3: [0, 1]
}

VECTORS_COUNT = 1000

INTERVAL = -5, 5

def get_corrective_coefficient(potential, decision):
    if potential > 0 :
        return 0 if decision else -1
    else :
        return 1 if decision else 0

def get_new_coefficients(vector, coefficients):
    coefficients = coefficients.copy()
    for (index, substitution_indexes) in SUBSTITUTION_INDEXES.items():
        coefficients[index] *= reduce((lambda x,y: x*y), map(lambda i: vector[i], substitution_indexes))
    return coefficients

def get_potential(coefficients, vector):
    result = coefficients[0]
    for (index, substitution_indexes) in SUBSTITUTION_INDEXES.items():
        result += coefficients[index] * reduce((lambda x,y: x*y), map(lambda i: vector[i], substitution_indexes))
    return result

def classify_random_vectors(coefficients):
    classified_data = defaultdict(list)
    for _ in range(VECTORS_COUNT):
        vector = (uniform(*INTERVAL), uniform(*INTERVAL))
        potential = get_potential(coefficients, vector)
        decision = potential > 0
        classified_data[decision].append(vector)
    return classified_data

def main():
    training_data_filename = os.path.dirname(os.path.abspath(__file__))+'\\linear.txt'#argv[1]
    training_data = load_training_data_from_file(training_data_filename)
    previous_coefficients = [0]*len(ERMIT_COEFFICIENTS)
    for (vector, decision) in training_data:
        potential = get_potential(previous_coefficients, vector)
        corrective_coefficient = get_corrective_coefficient(potential, decision)
        coefficients = get_new_coefficients(vector, ERMIT_COEFFICIENTS)
        coefficients = [x * corrective_coefficient for x in coefficients]
        result_coefficients = [x + y for x, y in zip(previous_coefficients, coefficients)]
        previous_coefficients = result_coefficients
    print(previous_coefficients)
    classified_data = classify_random_vectors(previous_coefficients)
    display_result(previous_coefficients, classified_data)

main()